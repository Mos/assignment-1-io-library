section .data
	buf : times 256 db 0

section .text


; Принимает код возврата и завершает текущий процесс
exit: 
    xor rdi, rdi
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
	.loop:
		cmp byte[rdi + rax], 0
		jz .exit
		inc rax
		jmp .loop
	.exit:
   ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    call string_length
	   mov rsi, rdi
	   mov rdx, rax
	   mov rax, 1
	   mov rdi, 1
	   syscall
	   xor rax, rax
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi 
    mov rsi, rsp ; stack
    mov rdi, 1 ; stdout
    mov rdx, 1 ; size
    mov rax, 1 ; sys_write
    syscall
    pop rdi ; pop symbol
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rax, 1
   	mov rdi, 1
		mov rsi, 10
		mov rdx, 1
		syscall
 xor rax, rax
 ret 

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
; rsp - stack head
; rdi - input number
    push r12
    push r13
    mov r12, rsp ; save stack to r12
    mov r13, 10 ; save the radix for division
    mov rax, rdi ; save number to rax
    dec rsp
    mov byte[rsp], 0 ; pop
	.loop:
		dec rsp ; move pointer
		xor rdx, rdx ; reset rdx
		div  r13 ; divide by 10
		add rdx, 0x30 ; to ASCII
		mov  byte[rsp], dl ; save to stack
		test rax, rax ; if rax = 0 then ZF = 0
		jz .print ; finish
		jmp .loop
	.print:
	 mov rdi, rsp
	 call print_string ; print to stdout
	 mov rsp, r12
 pop r13
 pop r12 ; restore registers
 ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
     xor rax, rax
	 ; rdi - input number
         mov rax, rdi ; save to rax
         test rax, rax ; set flags
         jns .plus ; if negative then write '-'
         mov  rdi, '-'
         push rax
         call print_char
         pop rax
         neg rax
         mov rdi, rax
         .plus:
         call print_uint 
         ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
      xor rcx, rcx
	.loop:
		mov r8b, byte [rdi+rcx]
		mov r9b, byte [rsi+rcx]
		cmp r8b, r9b
		jne .not_equals
		test r8b, r8b
		jz .equals
		inc rcx
		jmp .loop
	.equals:
	mov rax, 1
    ret
	.not_equals:
	mov rax, 0
	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push rdi
	push rdx
	dec rsp
	xor rax, rax ; syscall
	xor rdi, rdi ; stdin
	mov rdx, 1 ; word length
	mov rsi, rsp ; pointer
	syscall
	test rax, rax
	je .return
    xor rax, rax
	mov al, [rsp] ; save to rax
.return:
    inc rsp
	pop rdx
	pop rdi
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0x10.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера. 
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
  push rbx ; save rbx
	mov r8, rsi ; save buffer size
	mov r9, rdi ; save buffer address
  xor  rbx, rbx ; reset length
  xor  rdi, rdi ; stdin
  mov  rdx, 1 ; read size
  .skip:
    xor rax, rax
    mov rsi, buf
    syscall
      cmp al, 0 ; if string end then finish
      je  .finally
    cmp byte[buf], 0x21
    jb  .skip
  inc rbx
  .read:
    xor rax, rax
    lea rsi, [buf + rbx] ; set address to rsi
    syscall
    cmp byte [buf + rbx], 0x21 ; if end then finish
    jb  .finally
		cmp r8, rbx
		jbe .exit
    inc rbx
    jmp .read
  .finally:
  mov byte[buf + rbx], 0 ; return data
  mov rdx, rbx
  mov rax, buf
  pop rbx
  ret
	.exit:
	xor rdx, r8
	xor rax, rax
	pop rbx
	ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
	xor rsi, rsi
    mov r8, 10
    xor rcx, rcx
    xor rdx, rdx
	.loop:
    	mov sil, [rdi+rcx]
	    cmp sil, 0x30
    	jl .return
    	cmp sil, 0x39
    	jg .return
    	inc rcx
    	sub sil, 0x30
    	mul r8
    	add rax, rsi
    	jmp .loop
	.return:
    mov rdx, rcx
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    cmp byte [rdi], 0x2d
    je parse_ng
    call parse_uint
    ret
parse_ng:
    inc rdi
    call parse_uint
    cmp rdx, 0
    je .return
    neg rax
    inc rdx
.return:
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	xor rcx, rcx
	push r13
	.loop:
		cmp rcx, rdx
		je .too_long
		mov r13, [rdi + rcx]
		mov [rsi + rcx], r13
		cmp r13, 0
		je .exit
		inc rcx
		jmp .loop
	.too_long:
		mov rax, 0
	.exit:
		pop r13
		ret